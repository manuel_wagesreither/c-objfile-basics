#!/bin/sh

mkdir --parents out

echo "Preprocessing ..."
gcc -E main.c > out/main.preprocessed.c
gcc -E otherfile.c > out/otherfile.preprocessed.c


echo "Compiling ..."
# Compile, but do not assemble.
# output file is <inputfile>.s
# gcc -S out/main.preprocessed.c -Wa,-adhln -O0 -o out/main.s
gcc -S out/main.preprocessed.c -fverbose-asm -O0 -o out/main.s
gcc -S out/otherfile.preprocessed.c -fverbose-asm -O0 -o out/otherfile.s


echo "Assembling ..."
# Assemble, but do not link
# output file is <inputfile>.o
gcc -c out/main.s -o out/main.o
gcc -c out/otherfile.s -o out/otherfile.o


echo "Linking ..."
ld --relocatable out/main.o out/otherfile.o -o out/merged.o


echo "Final linking ..."
gcc -nostdlib -lgcc out/merged.o -o out/merged.out


echo "Writing listings ..."
objdump out/main.o --headers --wide > out/main.lst
objdump out/main.o --syms --wide >> out/main.lst
objdump out/main.o --reloc --wide >> out/main.lst
objdump out/main.o --disassemble-all --reloc --wide --section=.text --section=.data --source >> out/main.lst

objdump out/otherfile.o --headers --wide > out/otherfile.lst
objdump out/otherfile.o --syms --wide >> out/otherfile.lst
objdump out/otherfile.o --reloc --wide >> out/otherfile.lst
objdump out/otherfile.o --disassemble-all --reloc --wide --section=.text --section=.data --source >> out/otherfile.lst

objdump out/merged.o --headers --wide > out/merged.lst
objdump out/merged.o --syms --wide >> out/merged.lst
objdump out/merged.o --reloc --wide >> out/merged.lst
objdump out/merged.o --disassemble-all --reloc --wide --section=.text --section=.data --source >> out/merged.lst

objdump out/merged.out --headers --wide > out/merged.out.lst
objdump out/merged.out --syms --wide >> out/merged.out.lst
objdump out/merged.out --reloc --wide >> out/merged.out.lst
objdump out/merged.out --disassemble-all --reloc --wide --section=.text --section=.data --source >> out/merged.out.lst
