#include "otherfile.h"
#include "otherfile3.h"
#define FIRSTVAR (42+13)
#define SECONDVAR 2

int myvar;

int mainfilefun(int a, int b)
{
	int c;
	c = a - b;
	return c;
}

int main(int argc, char** argv)
{
	int a = FIRSTVAR;
	myvar = SECONDVAR;

	int sum;
	sum = otherfilefun(a, myvar);

	int diff;
	diff = mainfilefun(a, myvar);

	return 0;
}
